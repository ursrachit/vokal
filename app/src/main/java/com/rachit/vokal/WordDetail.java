package com.rachit.vokal;

/**
 * Created by rachit on 09/05/17.
 */

public class WordDetail {
    private int frequency=0;
    private String word;


    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public WordDetail(int frequency, String word) {
        this.frequency = frequency;
        this.word = word;
    }

    public void increaseFrequency(){
        this.frequency =this.frequency+1;
    }

}
