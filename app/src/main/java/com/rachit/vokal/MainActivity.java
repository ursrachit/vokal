package com.rachit.vokal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private int MY_INTENT_CLICK = 110;
    private TextView dataTv;
    HashMap<String, WordDetail> words;
    private ListView list;
    private WordListAdapter wAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button selectFileBtn = (Button) findViewById(R.id.selectFileBtn);
        dataTv = (TextView) findViewById(R.id.data_tv);
        list = (ListView) findViewById(android.R.id.list);

        selectFileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("text/plain"); //open only for text file
                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select file"), MY_INTENT_CLICK);
            }
        });


    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == MY_INTENT_CLICK) {
                if (data == null) return;
                Uri uri = data.getData();
                setText(uri);
            }
        }
    }

    private void setText(Uri uri) {
        BufferedReader br;
        try {
            br = new BufferedReader(new InputStreamReader(getContentResolver().openInputStream(uri)));

            String datatoread = "";
            String inBuffer = "";

            while ((datatoread = br.readLine()) != null) {
                inBuffer += datatoread + "\n";
            }

            Log.d("vokals ", inBuffer);
            useString(inBuffer);
            br.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void useString(String s) {
        //  dataTv.setText(s);
        words = new HashMap<String, WordDetail>();

        String[] allWords = s.split("[\\r\\n ,.?]+");

        String word;
        WordDetail wordDetail;
        // dataTv.setText(""+allWords.length);
        for (int i = 0; i < allWords.length; i++) {
            word = allWords[i].trim();
            if (words.get(word) == null) {
                wordDetail = new WordDetail(1, word);
                words.put(word, wordDetail);
            } else {
                wordDetail = words.get(word);
                wordDetail.increaseFrequency();
                words.put(word, wordDetail);
            }
        }

        Map<String, WordDetail> sortedMap = sortMap(words);
        //  printMap(sortedMap);
        setList(sortedMap);
        dataTv.setText("" + sortedMap.get("you").getFrequency());


    }

    //
    private static Map<String, WordDetail> sortMap(
            Map<String, WordDetail> unsortedMap) {

        List<Map.Entry<String, WordDetail>> list = new LinkedList<Map.Entry<String, WordDetail>>(
                unsortedMap.entrySet());

        Collections.sort(list,
                new Comparator<Map.Entry<String, WordDetail>>() {

                    @Override
                    public int compare(Map.Entry<String, WordDetail> o1,
                                       Map.Entry<String, WordDetail> o2) {
                        return o1.getValue().getFrequency() - (o2.getValue().getFrequency());
                    }
                });

        Map<String, WordDetail> sortedMap = new LinkedHashMap<String, WordDetail>();
        for (Map.Entry<String, WordDetail> item : list) {
            sortedMap.put(item.getKey(), item.getValue());
        }
        return sortedMap;
    }


    private void setList(Map<String, WordDetail> mp) {
        wAdapter = new WordListAdapter(this);
        int last = -1;

        for (Map.Entry<String, WordDetail> entry : mp.entrySet()) {
            Log.d("vokals ", entry.getKey() + " : " + entry.getValue().getFrequency());
            if (entry.getValue().getFrequency() > last) {
                last = (entry.getValue().getFrequency() / 10) * 10 + 10;
                int start = last - 9;
                wAdapter.addSeparatorItem(start + "-" + last + ":");
            }
            wAdapter.addItem(entry.getKey() + " : " + entry.getValue().getFrequency());
        }
        list.setAdapter(wAdapter);

    }

    //

}
